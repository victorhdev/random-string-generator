import java.util.Random;
import java.util.Scanner;

public class RandomString {
	public static void main(String[] args) {
		Scanner sn = new Scanner(System.in);
		System.out.println("Random String size:");
		int lenght = sn.nextInt();
		while(lenght > 62) {
			System.out.println("String size can not be higher than 62");
			lenght = sn.nextInt();
		}
		sn.close();
		System.out.println("Output: " + randomString(lenght));
	}
	
	public static String randomString(int lenght) {
		String character = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		String str = ""; 
		
		char[] text = new char[lenght];
		
		for(int i = 0;i<lenght;i++) {
			Random rnd = new Random();
			int n = rnd.nextInt(62);
			text[i] = character.charAt(n);
		}
		
		for(int i = 0;i<lenght;i++) {
			str += text[i];
		}
		return str;
	}
}
